CREATE DATABASE BaseRuben;
GO

USE BaseRuben;
GO

CREATE TABLE Users (
    id INT IDENTITY(1,1) PRIMARY KEY,
    user_email VARCHAR(255) NOT NULL UNIQUE,
    user_password VARCHAR(255) NOT NULL,
    user_name VARCHAR(255) NOT NULL,
    user_plan INT NULL,
    user_phone VARCHAR(20) NULL
);

-- Insertar usuarios

INSERT INTO Users (user_email, user_password, user_name, user_plan, user_phone)
VALUES ('usuarioexample1@sasf.net', 'contrasenia123', 'John Doe', 2, '095458545845');

INSERT INTO Users (user_email, user_password, user_name, user_plan, user_phone)
VALUES ('usuariosasf2@sasf.net', 'contrasenia123', 'Jane Doe', 1, '095458548485');

INSERT INTO Users (user_email, user_password, user_name, user_plan, user_phone)
VALUES ('usuariosasf3@sasf.net', 'contrasenia123', 'Alice Smith', NULL, '095458544515');

INSERT INTO Users (user_email, user_password, user_name, user_plan, user_phone)
VALUES ('usuariosasf4@sasf.net', 'contrasenia123', 'Bob Johnson', 3, '095458545215');

INSERT INTO Users (user_email, user_password, user_name, user_plan, user_phone)
VALUES ('usuariosasf5@sasf.net', 'contrasenia123', 'Emily Jackson', 2, '09545141744');


GO

CREATE TABLE Products (
    id INT IDENTITY(1,1) PRIMARY KEY,
    product_description VARCHAR(255) NOT NULL,
    product_price DECIMAL(10, 2) NOT NULL,
    product_status BIT NOT NULL,
    product_detail TEXT,
    product_image VARCHAR(255)
);
GO

INSERT INTO Products (product_description, product_price, product_status, product_detail, product_image)
VALUES ('Computadora gamer', 499.99, 1, 'Computadora AMD RYZEN 5.', 'https://example.com/images/lavadora.jpg');

INSERT INTO Products (product_description, product_price, product_status, product_detail, product_image)
VALUES ('Monitor ASUS pantalla oled', 799.95, 1, 'Monitor ultima tecnologia pantalla 27''.', 'https://example.com/images/refrigerador.jpg');

INSERT INTO Products (product_description, product_price, product_status, product_detail, product_image)
VALUES ('Horno de Microondas', 89.99, 1, 'Horno de microondas de 1000W con m�ltiples configuraciones de cocci�n.', 'https://example.com/images/microondas.jpg');

INSERT INTO Products (product_description, product_price, product_status, product_detail, product_image)
VALUES ('Televisor LED 4K', 549.99, 1, 'Televisor LED de 55 pulgadas con resoluci�n 4K y Smart TV integrado.', 'https://example.com/images/televisor.jpg');

INSERT INTO Products (product_description, product_price, product_status, product_detail, product_image)
VALUES ('Super-aspiradora de casa', 299.00, 1, 'Super-aspiradora de casa con navegaci�n inteligente y control remoto.', 'https://example.com/images/aspiradora.jpg');

INSERT INTO Products (product_description, product_price, product_status, product_detail, product_image)
VALUES ('Batidora de Pie', 49.95, 1, 'Batidora de pie con m�ltiples accesorios y velocidades.', 'https://example.com/images/batidora.jpg');
 
 
 CREATE PROCEDURE sp_login

    @user_email VARCHAR(255) = NULL,

    @user_password VARCHAR(255) = NULL

AS

BEGIN

    IF EXISTS (SELECT 1 FROM Users WHERE user_email = @user_email AND user_password = @user_password)

	BEGIN

        SELECT *,1 as access FROM Users WHERE user_email = @user_email;

	END

    ELSE

	BEGIN

        SELECT -1 AS access; 

	END

END;

CREATE PROCEDURE sp_products

AS

BEGIN

    SELECT * FROM Products;

    SELECT * FROM Products;

END;
