var cadena = WebApplication.CreateBuilder(args);

// Add services to the container.

cadena.Services.AddControllers();

cadena.Services.AddCors(options =>
{
    options.AddDefaultPolicy(cadena =>
    {
        cadena.WithOrigins("http://localhost:8080")
               .WithHeaders("Content-Type")
               .WithMethods("POST")
               .AllowAnyHeader()
               .AllowAnyMethod()
               .AllowCredentials();
    });
});

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
cadena.Services.AddEndpointsApiExplorer();
cadena.Services.AddSwaggerGen();

var app = cadena.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseCors();

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
