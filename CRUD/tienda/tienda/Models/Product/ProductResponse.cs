﻿namespace tienda.Models.Product
{
    public class ProductResponse
    {
        public bool Access { get; set; }

        public Product product { get; set; }
    }
}
