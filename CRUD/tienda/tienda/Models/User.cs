﻿using System.ComponentModel.DataAnnotations;

namespace tienda.Models
{
    public class User
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }

        [Required]
        public string Nombre { get; set; }

        [Required]
        public int Plan { get; set; }

        [Required]
        [Phone]
        public string Telefono { get; set; }
    }
}
