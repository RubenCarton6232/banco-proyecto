﻿namespace tienda.Models.Login
{
    public class LoginRequest
    {
        public String Email { get; set; }

        public String Password { get; set; }
    }
}
