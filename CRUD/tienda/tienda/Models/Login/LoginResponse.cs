﻿namespace tienda.Models.Login
{
    public class LoginResponse
    {
        public bool Access { get; set; }

        public User User { get; set; }
    }
}
