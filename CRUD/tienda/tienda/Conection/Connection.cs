﻿namespace tienda.Conection
{
    public class Connection
    {
        private string urlCon = string.Empty;

        public Connection()
        {
            var cadena = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();

            urlCon = cadena.GetSection("ConnectionStrings:Connection").Value;
        }

        public string getUrlCon()
        {
            return urlCon;
        }
        
    }
}
