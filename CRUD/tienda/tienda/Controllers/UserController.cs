﻿using Microsoft.AspNetCore.Mvc;
using tienda.Models.Login;
using tienda.Models.Product;
using tienda.Service;

namespace tienda.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : Controller
    {
        private readonly UserService _service;

        public UserController()
        {
            _service = new UserService();

        }

        [HttpPost]
        [ProducesResponseType(typeof(LoginResponse), 200)]
        public LoginResponse login([FromBody] LoginRequest loginRequest )
        {
            LoginResponse loginResponse = _service.login(loginRequest);

            return loginResponse;
        }
    }
}
