﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using tienda.Models.Login;
using tienda.Models.Product;
using tienda.Service;

namespace tienda.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : Controller
    {

        private readonly ProductService _service;

        public ProductController()
        {
            _service = new ProductService();

        }

        [HttpGet]
        [ProducesResponseType(typeof(List<Product>), 200)]
        public List<Product> ConsultarTodos()
        {
            List<Product> products = _service.getAllProduct();

            return products;
        }
    }
}
