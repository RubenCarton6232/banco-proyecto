﻿using System.Data.Common;
using tienda.Conection;
using System.Data.SqlClient;
using tienda.Models.Product;
using System.Data;

namespace tienda.Service
{
    public class ProductService
    {
        Connection connection = new Connection();

        public List<Product> getAllProduct()
        {
            using (DbConnection dbConnection = new SqlConnection(connection.getUrlCon()))
            {
                dbConnection.Open();
                using (DbCommand cmd = dbConnection.CreateCommand())
                {
                    cmd.CommandText = "sp_products";
                    cmd.CommandType = CommandType.StoredProcedure;

                    using (DbDataReader dr = cmd.ExecuteReader())
                    {
                        List<Product> products = new List<Product>();

                        while (dr.Read())
                        {
                            Product product = new Product();
                            product.Id = dr.GetInt32(dr.GetOrdinal("id"));
                            product.Descripcion = dr.GetString(dr.GetOrdinal("product_description"));
                            product.Precio = dr.GetDecimal(dr.GetOrdinal("product_price"));
                            product.Estado = dr.GetBoolean(dr.GetOrdinal("product_status"));
                            product.Detalle = dr.GetString(dr.GetOrdinal("product_detail"));
                            product.Imagen = dr.GetString(dr.GetOrdinal("product_image"));
                            products.Add(product);
                        }

                        return products;
                    }
                }
            }
        }
    }
}
