﻿using System.Data.Common;
using System.Data.SqlClient;
using System.Data;
using tienda.Conection;
using tienda.Models.Product;
using tienda.Models;
using tienda.Models.Login;
using System.Diagnostics.Eventing.Reader;

namespace tienda.Service
{
    public class UserService
    {
        Connection connection = new Connection();

        public LoginResponse login(LoginRequest loginRequest)
        {

            LoginResponse loginResponse = new LoginResponse();
            loginResponse.User = new User();

            using (DbConnection dbConnection = new SqlConnection(connection.getUrlCon()))
            {
                dbConnection.Open();

                using (DbCommand cmd = dbConnection.CreateCommand())
                {
                    cmd.CommandText = "sp_login";
                    cmd.CommandType = CommandType.StoredProcedure; 
                    cmd.Parameters.Add(new SqlParameter("@user_email", loginRequest.Email));

                    cmd.Parameters.Add(new SqlParameter("@user_password", loginRequest.Password));
                    
                    using (DbDataReader dr = cmd.ExecuteReader())
                    {

                        while (dr.Read())
                        {

                            loginResponse.Access = dr.GetInt32(dr.GetOrdinal("access")) != -1 ? true : false;
                            if (loginResponse.Access == true)
                            {
                                loginResponse.User.Id = dr.GetInt32(dr.GetOrdinal("id"));
                                loginResponse.User.Email = dr.GetString(dr.GetOrdinal("user_email"));
                                loginResponse.User.Password = dr.GetString(dr.GetOrdinal("user_password"));
                                loginResponse.User.Nombre = dr.GetString(dr.GetOrdinal("user_name"));
                                loginResponse.User.Plan = dr.GetInt32(dr.GetOrdinal("user_plan"));
                                loginResponse.User.Telefono = dr.GetString(dr.GetOrdinal("user_phone"));

                            }

                        }

                    }
                }
            }
            return loginResponse;
        }
    }
}
