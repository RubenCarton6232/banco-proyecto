import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Credencitals } from '../model/Model';
import { map } from 'rxjs';
import jwt_decode from 'jwt-decode';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(private http: HttpClient) { }
  data: any = {};

  signinAuth(creds: Credencitals) {
    const url = `${environment.urlLocal}`;
    this.data.transaccion= "autenticarUsuario";
    this.data.datosUsuario=creds; 
    console.log(this.data)
    return this.http.post(`${url}/usuarios`, this.data, { observe: 'response' }).pipe(
      map((response: HttpResponse<any>) => {
        const body = response.body;
        const headers = response.headers;
        const bearerToken = headers.get('Authorization')!;
        const token = bearerToken.replace('Bearer', '');
        localStorage.setItem('token', token);
        return body;
      })
    );
  }

  getToken(): string {
    return localStorage.getItem('token')!;
  }

  getDecodeToken() {
    return jwt_decode(this.getToken()) as { exp: number, role: string, sub: string };
  }
}
