import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthGuard } from '../../guard/auth.guard';
import { Credencitals } from '../../model/Model';
import { AuthService } from '../../service/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  form!: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private authService: AuthService
  ) {
    this.buildForm();
  }

  private buildForm() {
    this.form = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(4)]],
    })
  }

  sigin(event: Event) {
    event.preventDefault();
      const creds: Credencitals = this.form.value;
      console.log(creds)
      this.authService.signinAuth(creds)
        .subscribe(arg => {
          this.router.navigate(['/modulo-productos/productos'])
        });    

  }
}
