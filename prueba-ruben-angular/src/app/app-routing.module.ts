import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { AuthGuard } from './sesion/guard/auth.guard';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./sesion/sesion.module').then(m => m.SesionModule)
  },
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'modulo-productos',
    loadChildren: () => import('./modules/productos/productos-routing.module').then((m) => m.ProductosRoutingModule),
    
  },
  {
    path: 'login',
    loadChildren: () => import('./sesion/sesion.module').then(m => m.SesionModule)
  ,   canActivate:[AuthGuard],
  }]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
