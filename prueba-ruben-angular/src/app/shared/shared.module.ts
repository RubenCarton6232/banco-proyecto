import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AngularMaterialsModule } from './angularMaterials/angularMaterilas.module';


@NgModule({
  imports: [
    CommonModule,
    AngularMaterialsModule
  ],
  exports: [
   AngularMaterialsModule, CommonModule
  ]
})
export class SharedModule { }