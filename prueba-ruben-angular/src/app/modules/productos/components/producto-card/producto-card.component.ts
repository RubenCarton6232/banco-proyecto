import { Component, Input, OnInit } from '@angular/core';
import { Producto } from '../../interfaces/producto.interface';

@Component({
  selector: 'app-producto-card',
  templateUrl: './producto-card.component.html',
  styleUrls: ['./producto-card.component.scss']
})
export class ProductoCardComponent {
  @Input() producto: Producto = {
    id: 0,
    descripcion: '',
    precio: '',
    estado: false,
    detalle: '',
    imagen: ''
};
}
