import { Component, OnInit } from '@angular/core';
import { DataProductosService } from '../../services/productos.service';
import { Producto } from '../../interfaces/producto.interface';
import { Router } from '@angular/router';

@Component({
  selector: 'app-producto',
  templateUrl: './producto.component.html',
  styleUrls: ['./producto.component.scss']
})
export class ProductoComponent implements OnInit{
  productos: Producto[] = [];
  limit = 10;
  offset = 0;
  constructor(private productosService:DataProductosService,private router: Router){

  }

  ngOnInit(): void {
      this.getProductos();
  }

  getProductos(){
    this.productos = [];
    this.productosService.obtenerProductos().subscribe(data => {      
      this.productos = data;
      console.log(this.productos)
    })
  }  
}
