import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductosRoutingModule } from './productos-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { ProductoCardComponent } from './components/producto-card/producto-card.component';


@NgModule({
  declarations: [    
  
    ProductoCardComponent
  ],
  imports: [
    CommonModule,
    ProductosRoutingModule,    
    SharedModule
  ]
})
export class ProductosModule { }
