import { TestBed } from '@angular/core/testing';

import { DataProductosService } from './productos.service';

describe('ProductosService', () => {
  let service: DataProductosService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DataProductosService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
