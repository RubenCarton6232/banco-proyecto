import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Producto } from '../interfaces/producto.interface';
import { HttpClient, HttpHeaders,HttpParams  } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class DataProductosService {
  modificar =false;
  constructor(private http: HttpClient) { }

  obtenerProductos(): Observable<any> {    
    const url = `${environment.urlLocal}`;
    
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',        
        'Access-Control-Allow-Origin': '*'
      })      
    };
    
    return this.http.get(url, httpOptions);
  }

  // obtenerProducto(id: any): Observable<any> {    
  //   const url = `${environment.urlApi}/${id}`;
    
  //   const httpOptions = {
  //     headers: new HttpHeaders({
  //       'Content-Type': 'application/json',        
  //       'Access-Control-Allow-Origin': '*'
  //     })      
  //   };
    
  //   return this.http.get(url, httpOptions);
  // }

}