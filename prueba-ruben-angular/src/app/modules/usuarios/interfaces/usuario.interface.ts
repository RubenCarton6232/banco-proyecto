export interface Usuario {
    email: string;
    nombre: string;
    plan: number;
    telefono: string;
  }
  